CXX     ?= icpc
MPICXX  ?= mpic++
LINKS:=-lnetcdf -lhdf5_hl -lhdf5 -lz -lcurl
CFLAGS:=-O3 -fp-model fast=2 -Wall -fopenmp

DEBUG_FLAGS:=-g -DDEBUG
DEBUG_LDFLAGS:=-g

EXTRA_OPT_FLAGS:=-ip -ipo

DEBUG:=true
EXTRA_OPT:=false

ifeq ($(DEBUG),true)
	CFLAGS:=$(CFLAGS) $(DEBUG_FLAGS)
	LINKS:=$(LINKS) $(DEBUG_LDFLAGS)
endif

ifeq ($(EXTRA_OPT),true)
	CFLAGS:=$(CFLAGS) $(EXTRA_OPT_FLAGS)
endif

DERIVATIVES_CPPS := $(wildcard Derivatives/*.cpp)
DERIVATIVES_OBJS := $(addprefix Derivatives/,$(notdir $(DERIVATIVES_CPPS:.cpp=.o)))

NETCDF_IO_CPPS := $(wildcard NETCDF_IO/*.cpp)
NETCDF_IO_OBJS := $(addprefix NETCDF_IO/,$(notdir $(NETCDF_IO_CPPS:.cpp=.o)))

.PHONY: clean
clean:
	rm -f *.o 

all: solver.x tracker.x

%.o: %.cpp
	$(MPICXX) $(LINKS) -c $(CFLAGS) -o $@ $<

solver.x: MPI_split_comm.o ${NETCDF_IO_OBJS} ${DERIVATIVES_OBJS} solver.o
	$(MPICXX) $(LINKS) $(CFLAGS) -o $@ $^

tracker.x: MPI_split_comm.o ${NETCDF_IO_OBJS} interpolators.o tracker.o
	$(MPICXX) $(LINKS) $(CFLAGS) -o $@ $^
