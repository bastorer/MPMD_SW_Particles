#ifndef DERIVATIVES_HPP
#define DERIVATIVES_HPP 1

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <algorithm>
#include <math.h>
#include <mpi.h>
#include <omp.h>

void x_derivatives(double * dfdx, double * d2fdx2, double * f, 
          double * tmp_buffer, double * buffer_east, double * buffer_west,
          int my_Nx, int my_Ny, double dx,
          int bRank, int east_neighbour, int west_neighbour,
          MPI_Comm comm, MPI_Status *status, bool periodic, const char * var);

void y_derivatives(double * dfdy, double * d2fdy2, double * f, 
          double * tmp_buffer, double * buffer_south, double * buffer_north,
          int my_Nx, int my_Ny, double dy,
          int bRank, int south_neighbour, int north_neighbour,
          MPI_Comm comm, MPI_Status *status, bool periodic, const char * var);

#endif
