#include <stdio.h>
#ifndef INTERPOLATORS_HPP
#define INTERPOLATORS_HPP 1

#include <string.h>
#include <stdlib.h>
#include <math.h>

double cubic_interp(double * func, double xp, double yp, double dx, double dy, int Nx, int Ny, int nxp, int nyp, int Ppp);

double linear_interp(double * func, double xp, double yp, double dx, double dy, int Nx, int Ny, int nxp, int nyp, int Ppp);

#endif
