#include <fenv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <algorithm>
#include <mpi.h>
#include <omp.h>
#include <math.h>
#include "MPI_split_comm.hpp"
#include "netcdf_io.hpp"
#include "interpolators.hpp"

#ifndef DEBUG
    #define DEBUG false
#endif

int main( int argc, char *argv[] ) {

    const bool debug = DEBUG;

    // Enable all floating point exceptions but FE_INEXACT
    feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);

    // Specify the number of OpenMP threads
    //   and initialize the MPI world
    int tid, nthreads, thread_safety_provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &thread_safety_provided);

    // Get parameters for world communicators
    int wRank, wSize;
    MPI_Comm_rank( MPI_COMM_WORLD, &wRank );
    MPI_Comm_size( MPI_COMM_WORLD, &wSize );

    // Compute the binary(application)-specific communicator
    MPI_Comm binComm;
    int bRank, bSize;
    MPI_split_comm( argv[0], bRank, bSize, binComm );

    // Determine if the binary comm matches the world comm
    //   and set the coupled_flag accordingly
    int comm_comparison;
    MPI_Comm_compare( binComm, MPI_COMM_WORLD, &comm_comparison) ;
    bool coupled_flag = (comm_comparison == MPI_UNEQUAL);
    int root = bRank == 0 ? MPI_ROOT : MPI_PROC_NULL;

    if ( coupled_flag ) {
        // Print processor assignments
        if (bRank == 0) { fprintf(stdout, "Tracker code IS coupled.\n"); }
    } else if(bRank == 0) {
        fprintf(stdout, "Tracker code IS NOT coupled.\n");
    }

    MPI_Comm interComm;
    // Local rank 0 is the trackers's "root".  The remote root is the world's
    // "root" (0).  The tag (1234) is arbitrary.
    if (coupled_flag) { MPI_Intercomm_create( binComm, 0, MPI_COMM_WORLD, 0, 1234, &interComm ); }

    double *grid_x, *grid_y, *vel_u, *vel_v, *par_h, *tmp_f;
    double *my_particles_x, *my_particles_y, *my_particles_inert;
    double *my_particles_u,      *my_particles_v,      *my_particles_h;
    int number_grid_points_x, number_grid_points_y;
    double domain_length_x, domain_length_y;
    double time = 0., final_time, plot_time, next_plot_time, dt;
    int num_outs, Nproc_particles;
    int number_solver_procs_x, number_solver_procs_y;
    int number_particles, num_threads;
    read_initial_particles(
            &grid_x, &grid_y, &my_particles_x, &my_particles_y, &my_particles_inert,
            &my_particles_u, &my_particles_v,
            bRank, &number_grid_points_x, &number_grid_points_y, 
            &domain_length_x, &domain_length_y,
            &final_time, &num_outs,
            &number_solver_procs_x, &number_solver_procs_y, 
            &Nproc_particles,
            &num_threads, &number_particles,
            binComm );
    plot_time = final_time / num_outs;
    next_plot_time = plot_time;
    double dx = domain_length_x / number_grid_points_x;
    double dy = domain_length_y / number_grid_points_y;

    // Check if solver passed verification step
    int error_count = 0;
    if (coupled_flag) { MPI_Bcast(&error_count, 1, MPI_INT, 0, interComm); }
    if (error_count > 0) {
        if (bRank == 0) {
            fprintf(stdout, "Error_count > 1, halting\n");
        }
        MPI_Comm_free( &binComm );
        MPI_Comm_free( &interComm );
        MPI_Finalize();
        return 0;
    }

    // Specify number of threads for OpenMP
    omp_set_num_threads(num_threads);
    
    #pragma omp parallel default(shared) private(tid)
    {
        tid = omp_get_thread_num();
        nthreads = omp_get_num_threads();
        fprintf(stdout, "Hello from thread %d of %d on WORLD process %d/%d (running as %d/%d on %s)\n",
                tid, nthreads, wRank, wSize, bRank, bSize, argv[0] );
    }

    int Ppp = (number_grid_points_x * number_grid_points_y) / (number_solver_procs_x * number_solver_procs_y);
    int npx = number_solver_procs_x;
    int npy = number_solver_procs_y;

    // Iterator
    int ind;

    //
    //// Initialize paramteres and particle fields
    ////   hard-coded for the time being
    //
    int num_particles_per_proc = number_particles / bSize;
    double max_inertia = 1.;
    double min_inertia = 0.1; // very small inertia can cause numerical errors
                              // anything below this is set to inertia-less

    // Set the seed by the processor rank
    srand(bRank);

    double *my_particles_x_init, *my_particles_y_init;
    my_particles_h      = new double[num_particles_per_proc];
    my_particles_x_init = new double[num_particles_per_proc];
    my_particles_y_init = new double[num_particles_per_proc];

    double tmp_inert;

    for (int II = 0; II < num_particles_per_proc; II++) {
        my_particles_h[II] = 0.;

        my_particles_x_init[II] = my_particles_x[II];
        my_particles_y_init[II] = my_particles_y[II];
    }

    //
    //// Write particle positions to netcdf file
    //
    // Barrier to ensure that initial fields are written before particles
    MPI_Barrier(MPI_COMM_WORLD);
    int write_count = 0;
    write_particles_to_netcdf(coupled_flag, write_count, time, 
            my_particles_x, my_particles_y, my_particles_h, 
            my_particles_inert, my_particles_u, my_particles_v,
            my_particles_x_init, my_particles_y_init,
            bRank, num_particles_per_proc, number_particles, binComm);
    if (debug) { fprintf(stdout, "tracker %d/%d: wrote particle positions to netcdf file.\n", bRank, bSize); }


    //
    //// Initialize variables to store the grid and velocity fields
    //
    vel_u = new double[number_grid_points_x * number_grid_points_y];
    vel_v = new double[number_grid_points_x * number_grid_points_y];
    par_h = new double[number_grid_points_x * number_grid_points_y];
    tmp_f = new double[number_grid_points_x * number_grid_points_y];

    if (debug) { fprintf(stdout, "tracker %d/%d: Initialized grid and storage arrays.\n", bRank, bSize); }

    double min_x_pos = domain_length_x, min_y_pos = domain_length_y, max_x_pos=0., max_y_pos=0.;
    double u_interp, v_interp;
    double new_x, new_y;

    while (time < final_time) {

        //
        //// Receive velocity fields from solver
        //
        if (coupled_flag) {
            MPI_Bcast(&dt, 1, MPI_DOUBLE, 0, interComm);
        } else {
            dt = final_time / 10000.;
        }

        // MPI_Allgather seg faults when multiple tracker processors are present.
        //   To avoid this, MPI_Gather collects the velocity information to the
        //   tracker ROOT only. A tracker-wide MPI_Bcast then distributes the
        //   velocities to the other processors.
        // This approach seems to actually be quite expensive.
        if (bRank == 0) {
            if (coupled_flag) {
                // u
                MPI_Gather( NULL, 0,                                              MPI_DOUBLE, 
                        vel_u,       number_grid_points_x * number_grid_points_y, MPI_DOUBLE, 
                        root, interComm);
                // v
                MPI_Gather( NULL, 0,                                              MPI_DOUBLE, 
                        vel_v,       number_grid_points_x * number_grid_points_y, MPI_DOUBLE, 
                        root, interComm);
                // h
                MPI_Gather( NULL, 0,                                              MPI_DOUBLE, 
                        par_h,       number_grid_points_x * number_grid_points_y, MPI_DOUBLE, 
                        root, interComm);
            } else {
                for (int II = 0; II < number_grid_points_x; II++) {
                    for (int JJ = 0; JJ < number_grid_points_x; JJ++) {
                        vel_u[II + number_grid_points_x*JJ] =  0.1;
                        vel_v[II + number_grid_points_x*JJ] = -0.02;
                        par_h[II + number_grid_points_x*JJ] =  0.;
                    }
                }
            }
        }

        MPI_Bcast(vel_u, number_grid_points_x * number_grid_points_y, MPI_DOUBLE, 0, binComm);
        MPI_Bcast(vel_v, number_grid_points_x * number_grid_points_y, MPI_DOUBLE, 0, binComm);
        MPI_Bcast(par_h, number_grid_points_x * number_grid_points_y, MPI_DOUBLE, 0, binComm);

        if (debug) { fprintf(stdout, "tracker %d/%d: Received velocity fields.\n", bRank, bSize); }

        //
        //// Update particle positions
        ////   using a symplectic Euler scheme
        ////   and linear interpolation
        //

        #pragma omp parallel default(shared) private(ind, u_interp, v_interp, new_x, new_y)
        {
            #pragma omp for collapse(1) schedule(guided) reduction(max:max_x_pos) reduction(max:max_y_pos) \
                reduction(min:min_x_pos) reduction(min:min_y_pos)
            for (ind = 0; ind < num_particles_per_proc; ind++) {

                //
                //// Interpolate u_flow, update u_particle, update x_particle
                //
                u_interp = linear_interp( vel_u, my_particles_x[ind], my_particles_y[ind], dx, dy,
                        number_grid_points_x, number_grid_points_y, npx, npy, Ppp );

                if (my_particles_inert[ind] <= 0) { my_particles_u[ind] = u_interp; } 
                else                              { my_particles_u[ind] = u_interp + ( my_particles_u[ind] - u_interp ) * exp( - dt / my_particles_inert[ind]); }

                new_x = my_particles_x[ind] + dt * my_particles_u[ind];

                // Handle boundary conditions
                new_x -= floor( new_x / domain_length_x ) * domain_length_x;
                my_particles_x[ind] = new_x;

                //
                //// Interpolate v_flow, update v_particle, update y_particle
                //
                v_interp = linear_interp( vel_v, my_particles_x[ind], my_particles_y[ind], dx, dy, 
                        number_grid_points_x, number_grid_points_y, npx, npy, Ppp );

                if (my_particles_inert[ind] <= 0) { my_particles_v[ind] = v_interp; } 
                else                              { my_particles_v[ind] = v_interp + ( my_particles_v[ind] - v_interp ) * exp( - dt / my_particles_inert[ind]); }

                new_y = my_particles_y[ind] + dt * my_particles_v[ind];

                // Handle boundary conditions
                new_y -= floor( new_y / domain_length_y ) * domain_length_y;
                my_particles_y[ind] = new_y;

                // Update particle extrema for diagnostics
                max_x_pos = std::max( my_particles_x[ind], max_x_pos );
                min_x_pos = std::min( my_particles_x[ind], min_x_pos );

                max_y_pos = std::max( my_particles_y[ind], max_y_pos );
                min_y_pos = std::min( my_particles_y[ind], min_y_pos );

                // Interpolate h
                //   While not actually necessary, this can be useful
                //   for post-processing and graphics.
                my_particles_h[ind] = linear_interp( par_h, my_particles_x[ind], my_particles_y[ind], dx, dy,
                        number_grid_points_x, number_grid_points_y, npx, npy, Ppp );
            }
        }

        if (debug) { fprintf(stdout, "tracker %d/%d: Updated particle positions.\n", bRank, bSize); }
        MPI_Barrier(binComm);

        time += dt;

        //
        //// Write particle positions to netcdf file
        //
        if (time >= next_plot_time) {
            write_count++;
            write_particles_to_netcdf(coupled_flag, write_count, time,
                    my_particles_x, my_particles_y, my_particles_h,
                    my_particles_inert, my_particles_u, my_particles_v,
                    my_particles_x_init, my_particles_y_init,
                    bRank, num_particles_per_proc, number_particles, binComm);
            if (debug) { fprintf(stdout, "tracker %d/%d: wrote velocity fields to netcdf file.\n", bRank, bSize); }
            else if (bRank == 0) { 
                fprintf(stdout, "  tracker: \n"); 
                fprintf(stdout, "     min(x,y) = (%.16g, %.16g)\n", min_x_pos, min_y_pos); 
                fprintf(stdout, "     max(x,y) = (%.16g, %.16g)\n", max_x_pos, max_y_pos); 
            }
            next_plot_time += plot_time;
        }
    }


    //
    //// Close up shop
    //

    // Free the communicators 
    MPI_Barrier(MPI_COMM_WORLD);
    if (debug) { fprintf(stdout, "tracker %d/%d: Closing up shop.\n", bRank, bSize); }
    MPI_Comm_free( &binComm );
    MPI_Finalize();
    return 0;
}
