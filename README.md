
## Issues

1. Corner issues when using wall-wall boundary conditions
2. Inter-communication is done through a gather - broadcast combination, instead of an all-gather. This seemed neccessary to avoid seg-faults.
   1. Intra-communication on the tracker (broadcasting velocity field) is very expensive.
3. Wall boundary conditions have been implemented in the solver, but not tracker.
4. Cubic interpolation produces NaN values occasionally. Cause unknown.
5. Hybrid parallelization needs to be profiled and improved.
