#include <fenv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <algorithm>
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include "MPI_split_comm.hpp"
#include "netcdf_io.hpp"
#include "derivatives.hpp"

#ifndef DEBUG
    #define DEBUG false
#endif

int verify_solver( int app_rank, int app_size, int x_points, int y_points, int x_procs, int y_procs  ) {
    
    int error_count = 0;

    if ( ( x_points <= 0 ) or ( y_points <= 0 ) ) {
        if (app_rank == 0) { fprintf(stdout, "solver: Number of gridpoints invalid.\n"); }
        error_count++;
    } else if ( x_points % x_procs != 0 ) {
        if (app_rank == 0) { fprintf(stdout, "solver: Number of gridpoints in x must be divisible by number of processors in x.\n"); }
        error_count++;
    } else if ( y_points % y_procs != 0 ) {
        if (app_rank == 0) { fprintf(stdout, "solver: Number of gridpoints in y must be divisible by number of processors in y.\n"); }
        error_count++;
    } else if ( app_size != x_procs * y_procs) {
        if (app_rank == 0) { fprintf(stdout, "solver: Number of allocated processors does not match processor-grid size.\n"); }
        error_count++;
    }

    return error_count;

}

void compute_AB2_weights(double & AB_w, double & AB_w_prev, double tnow, double dt, double dt_prev) {

    double tp = tnow + dt;
    double t  = tnow;
    double tm = tnow - dt_prev;

    AB_w      = ( 0.5*(tp*tp - t*t) - tm*dt ) / (  dt_prev );
    AB_w_prev = ( 0.5*(tp*tp - t*t) - t*dt  ) / ( -dt_prev );

}

int main( int argc, char *argv[] ) {

    const bool debug = DEBUG;

    // Enable all floating point exceptions but FE_INEXACT
    feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);

    //
    //// Initialize communication protocols
    //

    // Specify the number of OpenMP threads
    //   and initialize the MPI world
    int tid, nthreads, thread_safety_provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &thread_safety_provided);
    MPI_Status status;

    // Get parameters for world communicators
    int wRank, wSize;
    MPI_Comm_rank( MPI_COMM_WORLD, &wRank );
    MPI_Comm_size( MPI_COMM_WORLD, &wSize );

    // Compute the binary(application)-specific communicator
    // bRank and bSize are determined in MPI_split_comm
    MPI_Comm binComm;
    int bRank, bSize;
    MPI_split_comm( argv[0], bRank, bSize, binComm );

    // Determine if the binary comm matches the world comm
    //   and set the coupled_flag accordingly
    int comm_comparison;
    MPI_Comm_compare( binComm, MPI_COMM_WORLD, &comm_comparison) ;
    bool coupled_flag = (comm_comparison == MPI_UNEQUAL);

    MPI_Comm interComm;
    // World rank 0 is the solver's "root".  The remote root is 1 past the last
    // rank in the solver's intracommunicator (bSize).  The tag (1234) is
    // arbitrary.
    if ( coupled_flag ) {
        if (bRank == 0) { fprintf(stdout, "Solver code IS coupled.\n"); }
        MPI_Intercomm_create( binComm, 0, MPI_COMM_WORLD, bSize, 1234, &interComm ); 
    } else if(bRank == 0) {
        fprintf(stdout, "Solver code IS NOT coupled.\n");
    }

    int root;
    if (coupled_flag) {
        root = bRank == 0 ? MPI_ROOT : MPI_PROC_NULL;
    }

    // Loop iterators
    int II, JJ;

    //
    //// Specify simulation parameters
    //// (to be passed as inputs later)
    //

    int number_grid_points_x, number_grid_points_y,
        number_processors_x,  number_processors_y,
        number_outputs,
        my_rank_x, my_rank_y,
        num_threads,
        my_Nx, my_Ny,
        num_threads_tracker, num_particles;

    double domain_length_x, domain_length_y,
           gravity, H0, coriolis_f, visco,
           final_time;

    // u -> m/s  ->  Zonal (East - West) velocity
    // v -> m/s  ->  Meridional (North - South) velocity
    // h -> m    ->  Total fluid depth
    // v -> 1/s  ->  Vorticity (dvdx - dudy)
    // x -> m    ->  Zonal (East - West) coordinate
    // y -> m    ->  Meridional (North - South) coordinate
    double *my_u, *my_v, *my_h, *my_x, *my_y, *my_vort;

    read_initial_conditions(
          &my_x, &my_y, &my_u, &my_v, &my_h, &my_vort,
          bRank, my_rank_x, my_rank_y,
          &H0, &gravity, &coriolis_f, &visco,
          &number_grid_points_x, &number_grid_points_y,
          &domain_length_x, &domain_length_y,
          my_Nx, my_Ny,
          &final_time, &number_outputs,
          &number_processors_x, &number_processors_y, &num_threads,
          &num_threads_tracker, &num_particles,
          binComm );
    if (debug and (bRank == 0)) { fprintf(stdout, "Read initial conditions and parameters.\n"); }
    omp_set_num_threads(num_threads);

    // Print processor assignments
    #pragma omp parallel default(shared) private(tid)
    {
        tid = omp_get_thread_num();
        nthreads = omp_get_num_threads();
        fprintf(stdout, "Hello from thread %d of %d on WORLD process %d/%d (running as %d/%d on %s)\n",
                tid, nthreads, wRank, wSize, bRank, bSize, argv[0] );
    }

    // Boundary conditions
    bool periodic_x = true; // true -> periodic, false -> free-slip
    bool periodic_y = true; // true -> periodic, false -> free-slip

    double dx = domain_length_x / number_grid_points_x;
    double dy = domain_length_y / number_grid_points_y;
    double c0 = pow(gravity * H0, 0.5);

    // CFL factor for determining dt
    //   and weights for AB2 time-stepping
    double CFL_factor = 0.2;
    double AB_w, AB_w_prev;

    // Temporal parameters
    double time = 0., dt=0., dt_prev=-1.;
    int    write_count = 0;
    double plot_time = final_time / number_outputs;
    double next_plot_time = plot_time;

    // Print simulation parameters
    if (bRank == 0) {
        fprintf(stdout, "Initializing simulation:\n");
        fprintf(stdout, "  Nx         = %d\n", number_grid_points_x);
        fprintf(stdout, "  Ny         = %d\n", number_grid_points_y);
        fprintf(stdout, "  Lx         = %.3g m\n", domain_length_x);
        fprintf(stdout, "  Ly         = %.3g m\n", domain_length_y);
        fprintf(stdout, "  Mean depth = %.3g m\n", H0);
        fprintf(stdout, "  gravity    = %.3g m/s/s\n", gravity);
        fprintf(stdout, "  f0         = %.3g /s\n", coriolis_f);
        fprintf(stdout, "  visco      = %.3g m^2/s\n", visco);
    }
    MPI_Barrier(binComm);

    int ind, iteration=0;

    // Verify parameters
    int error_count = verify_solver( bRank, bSize, number_grid_points_x, number_grid_points_y,
                                     number_processors_x, number_processors_y  );
    if (coupled_flag) {
        MPI_Bcast(&error_count, 1, MPI_INT, root, interComm);
    }
    if (error_count > 0) {
        MPI_Comm_free( &binComm );
        if ( coupled_flag ) { MPI_Comm_free( &interComm ); }
        MPI_Finalize();
        return 0;
    }

    //
    //// Variable initializations and processor-specific values
    ////    variables preceeded by my_ and understood to be 
    ////    local processor values
    //

    // Determine the rank of neighbouring processors
    //   these will be necessary for communicating edge values
    int west_neighbour  = (   (my_rank_x - 1 + number_processors_x) % number_processors_x  
                            +  my_rank_y      * number_processors_x 
                            + bSize )  % bSize;
    int east_neighbour  = (   (my_rank_x + 1 + number_processors_x) % number_processors_x  
                            +  my_rank_y      * number_processors_x )
                           % bSize;
    int north_neighbour = (    my_rank_x      
                            + (my_rank_y + 1 + number_processors_y) % number_processors_y * number_processors_x )
                           % bSize;
    int south_neighbour = (    my_rank_x      
                            + (my_rank_y - 1 + number_processors_y) % number_processors_y * number_processors_x 
                            + bSize )   % bSize;

    if (debug) {
        fprintf(stdout, "solver %d/%d:(%d, %d) -> (%d, %d, %d, %d)\n", 
                bRank, bSize, my_rank_x, my_rank_y,
                north_neighbour, east_neighbour, south_neighbour, west_neighbour);
    }

    // Declare an additional set to store information from the previous timestep (for AB2)
    double *my_dudt,      *my_dvdt,      *my_dhdt;
    double *my_dudt_prev, *my_dvdt_prev, *my_dhdt_prev;
    my_dudt      = new double[my_Nx * my_Ny];
    my_dvdt      = new double[my_Nx * my_Ny];
    my_dhdt      = new double[my_Nx * my_Ny];
    my_dudt_prev = new double[my_Nx * my_Ny];
    my_dvdt_prev = new double[my_Nx * my_Ny];
    my_dhdt_prev = new double[my_Nx * my_Ny];

    // Initialize fields for derivatives
    double *dudx, *dudy, *dhdx, *dvdx, *dvdy, *dhdy;
    double *d2udx2, *d2udy2, *d2vdx2, *d2vdy2;
    dudx   = new double[my_Nx * my_Ny];
    dvdx   = new double[my_Nx * my_Ny];
    dhdx   = new double[my_Nx * my_Ny];
    dudy   = new double[my_Nx * my_Ny];
    dvdy   = new double[my_Nx * my_Ny];
    dhdy   = new double[my_Nx * my_Ny];
    d2udx2 = new double[my_Nx * my_Ny];
    d2udy2 = new double[my_Nx * my_Ny];
    d2vdx2 = new double[my_Nx * my_Ny];
    d2vdy2 = new double[my_Nx * my_Ny];

    // Initialize ghost cells
    double *ghost_buffer_east,  *ghost_buffer_west,
           *ghost_buffer_north, *ghost_buffer_south,
           *tmp_buffer_EW,      *tmp_buffer_NS;
    tmp_buffer_EW      = new double[my_Ny];
    ghost_buffer_east  = new double[my_Ny];
    ghost_buffer_west  = new double[my_Ny];
    tmp_buffer_NS      = new double[my_Nx];
    ghost_buffer_north = new double[my_Nx];
    ghost_buffer_south = new double[my_Nx];
    
    // Compute initial vorticity
    if (debug and (bRank == 0)) { fprintf(stdout, "Computing initial vorticity\n"); }
    y_derivatives(dudy, NULL,  my_u, 
            tmp_buffer_NS, ghost_buffer_south, ghost_buffer_north,
            my_Nx, my_Ny, dy, bRank, south_neighbour, north_neighbour,
            binComm, &status, periodic_y, "u");

    x_derivatives(dvdx, NULL, my_v, 
            tmp_buffer_EW, ghost_buffer_east, ghost_buffer_west,
            my_Nx, my_Ny, dx, bRank, east_neighbour, west_neighbour,
            binComm, &status, periodic_x, "v");

    for (JJ = 0; JJ < my_Ny; JJ++) {
        for (II = 0; II < my_Nx; II++) {
            ind = II + JJ * my_Nx;
            my_vort[ind] = dvdx[ind] - dudy[ind];
        }
    }

    //
    //// Write initial variables to a file
    //
    if (debug and (bRank == 0)) { fprintf(stdout, "Writing initial state.\n"); }
    write_to_netcdf(write_count, time, 
            my_x, my_y,
            my_u, my_v, my_h, my_vort,
            bRank, my_rank_x, my_rank_y, 
            my_Nx, my_Ny, number_grid_points_x, number_grid_points_y,
            binComm);
    if (debug) { fprintf(stdout, "solver %d/%d: wrote velocity fields to netcdf file.\n", bRank, bSize); }

    // Barrier to ensure that initial fields are written before particles
    MPI_Barrier(MPI_COMM_WORLD);

    //
    //// Perform time integration
    //
    double max_u, max_v, my_dt;
    while (time < final_time) {

        // Determine dt
        max_u = 0;
        max_v = 0;
        #pragma omp parallel default(shared) private(ind) 
        {
            #pragma omp for collapse(2) reduction(max:max_u) reduction(max:max_v)
            for (JJ = 0; JJ < my_Ny; JJ++) {
                for (II = 0; II < my_Nx; II++) {
                    ind = II + JJ * my_Nx;
                    max_u = std::max( max_u, fabs(my_u[ind]) );
                    max_v = std::max( max_v, fabs(my_v[ind]) );
                }
            }
        }

        // Restriction from advection
        my_dt = std::min( dx / (c0 + 2*max_u), dy / (c0 + 2*max_v) );
        if (dt_prev <= 0 ) {  my_dt *= 0.1; }  // Reduce dt for Euler step

        // Restriction from viscosity
        if (visco > 0) {
            my_dt = CFL_factor * std::min( my_dt, std::min( dx*dx, dy*dy ) / visco  );
        }

        MPI_Allreduce(&my_dt, &dt, 1, MPI_DOUBLE, MPI_MIN, binComm);

        // Compute derivatives
        // du/dx and d2u/dx2
        x_derivatives(dudx, d2udx2, my_u, 
                tmp_buffer_EW, ghost_buffer_east, ghost_buffer_west,
                my_Nx, my_Ny, dx, bRank, east_neighbour, west_neighbour,
                binComm, &status, periodic_x, "u");

        // dv/dx and d2v/dx2
        x_derivatives(dvdx, d2vdx2, my_v, 
                tmp_buffer_EW, ghost_buffer_east, ghost_buffer_west,
                my_Nx, my_Ny, dx, bRank, east_neighbour, west_neighbour,
                binComm, &status, periodic_x, "v");

        // dh/dx
        x_derivatives(dhdx, NULL, my_h, 
                tmp_buffer_EW, ghost_buffer_east, ghost_buffer_west,
                my_Nx, my_Ny, dx, bRank, east_neighbour, west_neighbour,
                binComm, &status, periodic_x, "h");

        // du/dy and d2u/dy2
        y_derivatives(dudy, d2udy2,  my_u, 
                tmp_buffer_NS, ghost_buffer_south, ghost_buffer_north,
                my_Nx, my_Ny, dy, bRank, south_neighbour, north_neighbour,
                binComm, &status, periodic_y, "u");

        // dv/dy and d2v/dy2
        y_derivatives(dvdy, d2vdy2, my_v, 
                tmp_buffer_NS, ghost_buffer_south, ghost_buffer_north,
                my_Nx, my_Ny, dy, bRank, south_neighbour, north_neighbour,
                binComm, &status, periodic_y, "v");

        // dh/dy
        y_derivatives(dhdy, NULL, my_h, 
                tmp_buffer_NS, ghost_buffer_south, ghost_buffer_north,
                my_Nx, my_Ny, dy, bRank, south_neighbour, north_neighbour,
                binComm, &status, periodic_y, "h");

        //
        //// The derivatives have now been computed and stored in the appropriate arrays
        ////   now evolve the fields
        //

        // Get weights for AB2
        compute_AB2_weights(AB_w, AB_w_prev, time, dt, dt_prev);
        if (dt_prev <= 0) {
            AB_w      = dt;
            AB_w_prev = 0.;
        }

        #pragma omp parallel default(shared) private(II,JJ,ind)
        {
            #pragma omp for collapse(2) schedule(guided)
            for (JJ = 0; JJ < my_Ny; JJ++) {
                for (II = 0; II < my_Nx; II++) {

                    // Determine the linear index
                    ind = II + JJ * my_Nx;

                    // Store the d/dt fields
                    // du/dt + u du/dx + v du/dy + g * dh/dx - f0 * v - nu * (d2u/dx2 + d2u/dy2) = 0
                    my_dudt[ind] = my_u[ind]*dudx[ind] + my_v[ind]*dudy[ind] + gravity*dhdx[ind] - coriolis_f*my_v[ind]
                        - visco * ( d2udx2[ind] + d2udy2[ind] );

                    // dv/dt + u dv/dx + v dv/dy + g * dh/dy + f0 * u - nu * (d2v/dx2 + d2v/dy2) = 0
                    my_dvdt[ind] = my_u[ind]*dvdx[ind] + my_v[ind]*dvdy[ind] + gravity*dhdy[ind] + coriolis_f*my_u[ind]
                        - visco * ( d2vdx2[ind] + d2vdy2[ind] );

                    // dh/dt + d(uh)/dx + d(vh)/dy = 0
                    my_dhdt[ind] =   my_u[ind]*dhdx[ind] + dudx[ind]*my_h[ind]    // d(uh)/dx
                        + my_v[ind]*dhdy[ind] + dvdy[ind]*my_h[ind] ;  // d(vh)/dy

                    // Perform integration
                    my_u[ind] -= AB_w * my_dudt[ind] + AB_w_prev * my_dudt_prev[ind];
                    my_v[ind] -= AB_w * my_dvdt[ind] + AB_w_prev * my_dvdt_prev[ind];
                    my_h[ind] -= AB_w * my_dhdt[ind] + AB_w_prev * my_dhdt_prev[ind];

                    // Compute vorticity
                    my_vort[ind] = dvdx[ind] - dudy[ind];
                }
            }
        }

        time += dt;
        iteration++;

        //
        //// Write output as necessary
        //
        if (time >= next_plot_time) {
            write_count++;
            write_to_netcdf(write_count, time, 
                    my_x, my_y,
                    my_u, my_v, my_h, my_vort,
                    bRank, my_rank_x, my_rank_y, 
                    my_Nx, my_Ny, number_grid_points_x, number_grid_points_y,
                    binComm);
            if (debug) { fprintf(stdout, "solver %d/%d: wrote velocity fields to netcdf file.\n", bRank, bSize); }
            else if (bRank == 0) { 
                fprintf(stdout, "solver: completed output %d of %g\n", write_count, final_time/plot_time); 
                fprintf(stdout, "        t = %.8gs ( / %.8gs )\n", time, final_time); 
                fprintf(stdout, "        approx. %.4g%% complete\n", 100.*time/final_time); 
                fprintf(stdout, "        used %d iterations so far\n", iteration); 
            }
            next_plot_time += plot_time;
        }
        //MPI_Barrier(binComm);

        //
        //// Communicate velocity fields to tracker (if applicable)
        //
        if ( coupled_flag ) {
            MPI_Bcast(&dt, 1, MPI_DOUBLE, root, interComm);

            // MPI_Allgather seg faults when multiple tracker processors are present.
            //   To avoid this, MPI_Gather collects the velocity information to the
            //   tracker ROOT only. A tracker-wide MPI_Bcast then distributes the
            //   velocities to the other processors.
            // This increases the amount of intracommunication happening on the tracker
            //   side, but should decrease the amount of intercommunication, which may
            //   be more beneficial in the long run (?)
            MPI_Gather( my_u, my_Nx * my_Ny, MPI_DOUBLE, 
                    NULL, 0,             MPI_DOUBLE, 
                    0, interComm);

            MPI_Gather( my_v, my_Nx * my_Ny, MPI_DOUBLE, 
                    NULL, 0,             MPI_DOUBLE, 
                    0, interComm);

            MPI_Gather( my_h, my_Nx * my_Ny, MPI_DOUBLE, 
                    NULL, 0,             MPI_DOUBLE, 
                    0, interComm);

            if (debug) { fprintf(stdout, "solver %d/%d: Sent velocity fields.\n", bRank, bSize); }
        }

        // Update previous information to continue AB2
        //   to avoid copying information, just permute
        //   the pointers
        dt_prev = dt;
        std::swap(my_dudt, my_dudt_prev);
        std::swap(my_dvdt, my_dvdt_prev);
        std::swap(my_dhdt, my_dhdt_prev);

    }

    //
    //// Close up shop
    //

    // Free the communicators
    MPI_Barrier(MPI_COMM_WORLD);
    if (debug) { fprintf(stdout, "solver %d/%d: Closing up shop.\n", bRank, bSize); }
    if ( coupled_flag ) { MPI_Comm_free( &interComm ); }
    MPI_Comm_free( &binComm );
    MPI_Finalize();
    return 0;
}
