#include "MPI_split_comm.hpp"

void MPI_split_comm( char* binName, int & bRank, int & bSize, MPI_Comm & binComm ) {

    int wRank, wSize;
    MPI_Comm_rank( MPI_COMM_WORLD, &wRank );
    MPI_Comm_size( MPI_COMM_WORLD, &wSize );

    int myLen = strlen( binName ) + 1;
    int maxLen;
    // Gathering the maximum length of the executable' name
    MPI_Allreduce( &myLen, &maxLen, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD );

    // Allocating memory for all of them
    //char *names = malloc( wSize * maxLen );
    char * names;
    names = new char[wSize * maxLen];
    // and copying my name at its place in the array
    strcpy( names + ( wRank * maxLen ), binName );

    // Now collecting all executable' names
    MPI_Allgather( MPI_IN_PLACE, 0, MPI_DATATYPE_NULL,
            names, maxLen, MPI_CHAR, MPI_COMM_WORLD );

    // With that, I can sort-out who is executing the same binary as me
    int binIdx = 0;
    while( strcmp( binName, names + binIdx * maxLen ) != 0 ) {
        binIdx++;
    }
    free( names );

    // Now, all processes with the same binIdx value are running the same binary
    // split MPI_COMM_WORLD accordingly
    MPI_Comm_split( MPI_COMM_WORLD, binIdx, wRank, &binComm );

    MPI_Comm_rank( binComm, &bRank );
    MPI_Comm_size( binComm, &bSize );

}
