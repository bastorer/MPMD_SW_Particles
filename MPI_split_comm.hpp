#ifndef MPI_SPLIT_COMM_HPP
#define MPI_SPLIT_COMM_HPP 1

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mpi.h>

void MPI_split_comm( char* binName, int & bRank, int & bSize, MPI_Comm & binComm );

#endif
