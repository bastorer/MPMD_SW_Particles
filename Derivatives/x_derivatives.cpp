#include "../derivatives.hpp" 

void x_derivatives(double * dfdx, double * d2fdx2, double * f, 
          double * tmp_buffer, double * buffer_east, double * buffer_west,
          int my_Nx, int my_Ny, double dx,
          int bRank, int east_neighbour, int west_neighbour,
          MPI_Comm comm, MPI_Status *status, 
          bool periodic, const char * var) {

    int II, JJ;

    // Send westwards
    for (JJ = 0; JJ < my_Ny; JJ++) { tmp_buffer[JJ] = f[0 + JJ*my_Nx]; }
    MPI_Sendrecv(tmp_buffer,  my_Ny, MPI_DOUBLE, west_neighbour, bRank,
                 buffer_east, my_Ny, MPI_DOUBLE, east_neighbour, east_neighbour,
                 comm, status);
    // Send eastwards
    for (JJ = 0; JJ < my_Ny; JJ++) { tmp_buffer[JJ] = f[my_Nx-1 + JJ*my_Nx]; }
    MPI_Sendrecv(tmp_buffer,  my_Ny, MPI_DOUBLE, east_neighbour, bRank,
                 buffer_west, my_Ny, MPI_DOUBLE, west_neighbour, west_neighbour,
                 comm, status);

    // Impose non-periodic conditions if necessary
    if (!periodic) {
        if (west_neighbour >= bRank) {
            if (strcmp(var, "u") == 0) { for (JJ = 0; JJ < my_Ny; JJ++) { buffer_west[JJ] = -2*f[0 + JJ*my_Nx] + (1./3)*f[1 + JJ*my_Nx];                   } } 
            else                       { for (JJ = 0; JJ < my_Ny; JJ++) { buffer_west[JJ] =  3*f[0 + JJ*my_Nx] -      3*f[1 + JJ*my_Nx] + f[2 + JJ*my_Nx]; } }
        }
        if (east_neighbour <= bRank) {
            if (strcmp(var, "u") == 0) { for (JJ = 0; JJ < my_Ny; JJ++) { buffer_east[JJ] = -2*f[my_Nx-1 + JJ*my_Nx] + (1./3)*f[my_Nx-2 + JJ*my_Nx];                         } } 
            else                       { for (JJ = 0; JJ < my_Ny; JJ++) { buffer_east[JJ] =  3*f[my_Nx-1 + JJ*my_Nx] -      3*f[my_Nx-2 + JJ*my_Nx] + f[my_Nx-3 + JJ*my_Nx]; } }
        }
    }

    // Compute first derivative
    #pragma omp parallel default(shared) private(II,JJ)
    {
        #pragma omp for collapse(1) schedule(guided)
        for (JJ = 0; JJ < my_Ny; JJ++) { dfdx[0 + JJ*my_Nx] = ( f[1 + JJ*my_Nx] - buffer_west[JJ] ) / ( 2*dx ); }
    }
    #pragma omp parallel default(shared) private(II,JJ)
    {
        #pragma omp for collapse(2) schedule(guided)
        for (JJ = 0; JJ < my_Ny; JJ++) {
            for (II = 1; II < my_Nx-1; II++) {
                dfdx[II + JJ*my_Nx] = ( f[II+1 + JJ*my_Nx] - f[II-1 + JJ*my_Nx] ) / ( 2*dx );
            }
        }
    }
    #pragma omp parallel default(shared) private(II,JJ)
    {
        #pragma omp for collapse(1) schedule(guided)
        for (JJ = 0; JJ < my_Ny; JJ++) { dfdx[my_Nx-1 + JJ*my_Nx] = ( buffer_east[JJ] - f[my_Nx-2 + JJ*my_Nx] ) / ( 2*dx ); }
    }

    // Compute second derivative, if required
    if (d2fdx2) {

        // Update non-periodic conditions if necessary
        if (!periodic) {
            if (west_neighbour >= bRank) {
                if (strcmp(var, "u") == 0) { for (JJ = 0; JJ < my_Ny; JJ++) { buffer_west[JJ] = -  2*f[0 + JJ*my_Nx] + (1./3)*f[1 + JJ*my_Nx];                       } } 
                else                       { for (JJ = 0; JJ < my_Ny; JJ++) { buffer_west[JJ] =  2.5*f[0 + JJ*my_Nx] -      2*f[1 + JJ*my_Nx] + 0.5*f[2 + JJ*my_Nx]; } }
            }
            if (east_neighbour <= bRank) {
                if (strcmp(var, "u") == 0) { for (JJ = 0; JJ < my_Ny; JJ++) { buffer_east[JJ] = -  2*f[my_Nx-1 + JJ*my_Nx] + (1./3)*f[my_Nx-2 + JJ*my_Nx];                             } } 
                else                       { for (JJ = 0; JJ < my_Ny; JJ++) { buffer_east[JJ] =  2.5*f[my_Nx-1 + JJ*my_Nx] -      2*f[my_Nx-2 + JJ*my_Nx] + 0.5*f[my_Nx-3 + JJ*my_Nx]; } }
            }
        }

        // Derivative calculation
        #pragma omp parallel default(shared) private(II,JJ)
        {
            #pragma omp for collapse(1) schedule(guided)
            for (JJ = 0; JJ < my_Ny; JJ++) { d2fdx2[0 + JJ*my_Nx] = ( f[1 + JJ*my_Nx] - 2*f[0 + JJ*my_Nx] + buffer_west[JJ] ) / ( dx*dx ); }
        }
        #pragma omp parallel default(shared) private(II,JJ)
        {
            #pragma omp for collapse(2) schedule(guided)
            for (JJ = 0; JJ < my_Ny; JJ++) {
                for (II = 1; II < my_Nx-1; II++) {
                    d2fdx2[II + JJ*my_Nx] = ( f[II+1 + JJ*my_Nx] - 2*f[II + JJ*my_Nx] + f[II-1 + JJ*my_Nx] ) / ( dx*dx );
                }
            }
        }
        #pragma omp parallel default(shared) private(II,JJ)
        {
            #pragma omp for collapse(1) schedule(guided)
            for (JJ = 0; JJ < my_Ny; JJ++) { d2fdx2[my_Nx-1 + JJ*my_Nx] = ( buffer_east[JJ] - 2*f[my_Nx-1+JJ*my_Nx]  + f[my_Nx-2 + JJ*my_Nx] ) / ( dx*dx ); }
        }
    }
}


