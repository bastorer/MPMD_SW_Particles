#include "../derivatives.hpp" 

void y_derivatives(double * dfdy, double * d2fdy2, double * f, 
          double * tmp_buffer, double * buffer_south, double * buffer_north,
          int my_Nx, int my_Ny, double dy,
          int bRank, int south_neighbour, int north_neighbour,
          MPI_Comm comm, MPI_Status *status, 
          bool periodic, const char * var) {

    int II, JJ;

    // Send southwards
    for (II = 0; II < my_Nx; II++) { tmp_buffer[II] = f[II + 0*my_Nx]; }
    MPI_Sendrecv(tmp_buffer,   my_Nx, MPI_DOUBLE, south_neighbour, bRank,
                 buffer_north, my_Nx, MPI_DOUBLE, north_neighbour, north_neighbour,
                 comm, status);
    // Send northwards
    for (II = 0; II < my_Nx; II++) { tmp_buffer[II] = f[II + (my_Ny-1)*my_Nx]; }
    MPI_Sendrecv(tmp_buffer,   my_Nx, MPI_DOUBLE, north_neighbour, bRank,
                 buffer_south, my_Nx, MPI_DOUBLE, south_neighbour, south_neighbour,
                 comm, status);

    // Impose non-periodic conditions if necessary
    if (!periodic) {
        if (north_neighbour <= bRank) {
            if (strcmp(var, "v") == 0) { for (II = 0; II < my_Nx; II++) { buffer_north[II] = -2*f[II + (my_Ny-1)*my_Nx] + (1./3)*f[II + (my_Ny-2)*my_Nx]; } } 
            else                       { for (II = 0; II < my_Nx; II++) { buffer_north[II] =  3*f[II + (my_Ny-1)*my_Nx] - 3*f[II + (my_Ny-2)*my_Nx] + f[II + (my_Ny-3)*my_Nx]; } }
        }
        if (south_neighbour >= bRank) {
            if (strcmp(var, "v") == 0) { for (II = 0; II < my_Nx; II++) { buffer_south[II] = -2*f[II + 0*my_Nx] + (1./3)*f[II + 1*my_Nx]; } } 
            else                       { for (II = 0; II < my_Nx; II++) { buffer_south[II] =  3*f[II + 0*my_Nx] - 3*f[II + 1*my_Nx] + f[II + 2*my_Nx]; } }
        }
    }

    // Compute first derivative
    #pragma omp parallel default(shared) private(II,JJ)
    {
        #pragma omp for collapse(1) schedule(guided)
        for (II = 0; II < my_Nx; II++)     { dfdy[II + 0*my_Nx]           = ( f[II + (1)*my_Nx]    - buffer_south[II]          ) / ( 2*dy ); }
    }
    #pragma omp parallel default(shared) private(II,JJ)
    {
        #pragma omp for collapse(2) schedule(guided)
        for (JJ = 1; JJ < my_Ny-1; JJ++) {
            for (II = 0; II < my_Nx; II++) { dfdy[II + JJ*my_Nx]          = ( f[II + (JJ+1)*my_Nx] - f[II + (JJ-1)*my_Nx]      ) / ( 2*dy ); }
        }
    }
    #pragma omp parallel default(shared) private(II,JJ)
    {
        #pragma omp for collapse(1) schedule(guided)
        for (II = 0; II < my_Nx; II++)     { dfdy[II + (my_Ny - 1)*my_Nx] = ( buffer_north[II]     - f[II + (my_Ny - 2)*my_Nx] ) / ( 2*dy ); }
    }


    // Compute second derivative, if required
    if (d2fdy2) {
        // Impose non-periodic conditions if necessary
        if (!periodic) {
            if (north_neighbour <= bRank) {
                if (strcmp(var, "v") == 0) { for (II = 0; II < my_Nx; II++) { buffer_north[II] = -2*f[II + (my_Ny-1)*my_Nx] + (1./3)*f[II + (my_Ny-2)*my_Nx]; } } 
                else                       { for (II = 0; II < my_Nx; II++) { buffer_north[II] =  2.5*f[II + (my_Ny-1)*my_Nx] - 2*f[II + (my_Ny-2)*my_Nx] + 0.5*f[II + (my_Ny-3)*my_Nx]; } }
            }
            if (south_neighbour >= bRank) {
                if (strcmp(var, "v") == 0) { for (II = 0; II < my_Nx; II++) { buffer_south[II] = -2*f[II + 0*my_Nx] + (1./3)*f[II + 1*my_Nx]; } } 
                else                       { for (II = 0; II < my_Nx; II++) { buffer_south[II] =  2.5*f[II + 0*my_Nx] - 2*f[II + 1*my_Nx] + 0.5*f[II + 2*my_Nx]; } }
            }
        }

        // Derivative calculation
        #pragma omp parallel default(shared) private(II,JJ)
        {
            #pragma omp for collapse(1) schedule(guided)
            for (II = 0; II < my_Nx; II++)     { d2fdy2[II + 0*my_Nx]           = ( f[II + (1)*my_Nx]    - 2*f[II + (0)*my_Nx]         + buffer_south[II]          ) / ( dy*dy ); }
        }
        #pragma omp parallel default(shared) private(II,JJ)
        {
            #pragma omp for collapse(2) schedule(guided)
            for (JJ = 1; JJ < my_Ny-1; JJ++) {
                for (II = 0; II < my_Nx; II++) { d2fdy2[II + JJ*my_Nx]          = ( f[II + (JJ+1)*my_Nx] - 2*f[II + JJ*my_Nx]          + f[II + (JJ-1)*my_Nx]      ) / ( dy*dy ); }
            }
        }
        #pragma omp parallel default(shared) private(II,JJ)
        {
            #pragma omp for collapse(1) schedule(guided)
            for (II = 0; II < my_Nx; II++)     { d2fdy2[II + (my_Ny - 1)*my_Nx] = ( buffer_north[II]     - 2*f[II + (my_Ny - 1)*my_Nx] + f[II + (my_Ny - 2)*my_Nx] ) / ( dy*dy ); }
        }
    }
}
