#ifndef NETCDF_IO_HPP
#define NETCDF_IO_HPP 1

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "netcdf.h"
#include "netcdf_par.h"
#include "hdf5.h"

void NC_ERR(int e);

void write_to_netcdf(int iter, double curr_time, 
        double * my_x, double * my_y,
        double * my_u, double * my_v, double * my_h, double * my_vort,
        int bRank, int my_rank_x, int my_rank_y, 
        int my_Nx, int my_Ny, int full_Nx, int full_Ny,
        MPI_Comm comm_world);

void write_particles_to_netcdf(bool coupled_flag, int iter, double curr_time, 
        double * my_particle_x, double * my_particle_y, double * my_particle_h,
        double * my_particle_inert, double * my_particle_u, double * my_particle_v,
        double * my_particle_x_init, double * my_particle_y_init,
        int bRank, int number_parts_per_proc, int number_parts_total,
        MPI_Comm comm_world );

void read_initial_conditions(
        double ** my_x, double ** my_y,
        double ** my_u, double ** my_v, double ** my_h, double ** my_vort,
        int bRank, int & my_rank_x, int & my_rank_y, 
        double * H0, double * g, double * f0, double * visco,
        int * Nx, int * Ny, double * Lx, double * Ly,
        int & my_Nx, int & my_Ny,
        double * final_time, int * num_outs,
        int * Nproc_x, int * Nproc_y, int * Nthreads,
        int * Nthreads_tracker, int * num_particles,
        MPI_Comm comm_world );

void read_initial_particles(
        double ** my_x, double ** my_y,
        double ** my_part_x, double ** my_part_y, double ** my_part_inert,
        double ** my_part_u, double ** my_part_v,
        int bRank, int * Nx, int * Ny, double * Lx, double * Ly,
        double * final_time, int * num_outs,
        int * Nproc_x, int * Nproc_y, int * Nproc_part,
        int * Nthreads_tracker, int * num_particles,
        MPI_Comm comm_world );

#endif
