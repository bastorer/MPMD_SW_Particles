#include "../netcdf_io.hpp"

// Write to netcdf file
void write_particles_to_netcdf(bool coupled_flag, int iter, double curr_time, 
        double * my_particle_x, double * my_particle_y, double * my_particle_h,
        double * my_particle_inert, double * my_particle_u, double * my_particle_v,
        double * my_particle_x_init, double * my_particle_y_init,
        int bRank, int number_parts_per_proc, int number_parts_total,
        MPI_Comm comm_world ) {

    // Open the NETCDF file
    int ncid=0, retval, FLAG;
    char buffer [50];
    snprintf(buffer, 50, "output_%04d.nc", iter);
    if (!coupled_flag) {
        FLAG = NC_NETCDF4 | NC_MPIIO | NC_CLOBBER;
        if (( retval = nc_create_par(buffer, FLAG, comm_world, MPI_INFO_NULL, &ncid) )) { NC_ERR(retval); }
    } else {
        FLAG = NC_NETCDF4 | NC_MPIIO | NC_WRITE;
        if (( retval = nc_open_par(buffer, FLAG, comm_world, MPI_INFO_NULL, &ncid) )) { NC_ERR(retval); }
    }

    // Define the dimensions
    int trajdimid;
    if ((retval = nc_def_dim(ncid, "trajectory", number_parts_total, &trajdimid)))
        NC_ERR(retval);

    // Define coordinate variables
    int trajvarid;
    if ((retval = nc_def_var(ncid, "trajectory", NC_DOUBLE, 1, &trajdimid, &trajvarid)))
        NC_ERR(retval);

    // Transpose
    const int ndims = 1;
    int dimids[ndims];
    dimids[0] = trajdimid;

    // Declare variables
    int part_xvarid, part_yvarid, part_hvarid;
    int part_inertvarid, part_uvarid, part_vvarid;
    int part_xivarid, part_yivarid;
    if ((retval = nc_def_var(ncid, "particle_x", NC_DOUBLE, ndims, dimids, &part_xvarid)))
        NC_ERR(retval);
    if ((retval = nc_def_var(ncid, "particle_y", NC_DOUBLE, ndims, dimids, &part_yvarid)))
        NC_ERR(retval);
    if ((retval = nc_def_var(ncid, "particle_h", NC_DOUBLE, ndims, dimids, &part_hvarid)))
        NC_ERR(retval);
    if ((retval = nc_def_var(ncid, "particle_inertia", NC_DOUBLE, ndims, dimids, &part_inertvarid)))
        NC_ERR(retval);
    if ((retval = nc_def_var(ncid, "particle_u", NC_DOUBLE, ndims, dimids, &part_uvarid)))
        NC_ERR(retval);
    if ((retval = nc_def_var(ncid, "particle_v", NC_DOUBLE, ndims, dimids, &part_vvarid)))
        NC_ERR(retval);
    if ((retval = nc_def_var(ncid, "init_particle_x", NC_DOUBLE, ndims, dimids, &part_xivarid)))
        NC_ERR(retval);
    if ((retval = nc_def_var(ncid, "init_particle_y", NC_DOUBLE, ndims, dimids, &part_yivarid)))
        NC_ERR(retval);

    // Put the coordinate variables
    size_t start[1], count[1];
    start[0] = number_parts_per_proc * bRank;
    count[0] = number_parts_per_proc;
    double *my_traj;
    my_traj = new double[number_parts_per_proc];
    for (int II = 0; II < number_parts_per_proc; II++) {
        my_traj[II] = number_parts_per_proc * bRank + II;
    }

    if ((retval = nc_put_vara_double(ncid, trajvarid, start, count, my_traj)))
        NC_ERR(retval);

    if ((retval = nc_put_vara_double(ncid, part_xvarid, start, count, my_particle_x)))
        NC_ERR(retval);
    if ((retval = nc_put_vara_double(ncid, part_yvarid, start, count, my_particle_y)))
        NC_ERR(retval);
    if ((retval = nc_put_vara_double(ncid, part_hvarid, start, count, my_particle_h)))
        NC_ERR(retval);
    if ((retval = nc_put_vara_double(ncid, part_inertvarid, start, count, my_particle_inert)))
        NC_ERR(retval);
    if ((retval = nc_put_vara_double(ncid, part_uvarid, start, count, my_particle_u)))
        NC_ERR(retval);
    if ((retval = nc_put_vara_double(ncid, part_vvarid, start, count, my_particle_v)))
        NC_ERR(retval);
    if ((retval = nc_put_vara_double(ncid, part_xivarid, start, count, my_particle_x_init)))
        NC_ERR(retval);
    if ((retval = nc_put_vara_double(ncid, part_yivarid, start, count, my_particle_y_init)))
        NC_ERR(retval);

    // Close the file
    if ((retval = nc_close(ncid))) { NC_ERR(retval); }

}
