#include "../netcdf_io.hpp"

// Write to netcdf file
void read_initial_conditions(
        double ** my_x, double ** my_y,
        double ** my_u, double ** my_v, double ** my_h, double ** my_vort,
        int bRank, int & my_rank_x, int & my_rank_y, 
        double * H0, double * g, double * f0, double * visco,
        int * Nx, int * Ny, double * Lx, double * Ly,
        int & my_Nx, int & my_Ny,
        double * final_time, int * num_outs,
        int * Nproc_x, int * Nproc_y, int * Nthreads,
        int * Nthreads_tracker, int * num_particles,
        MPI_Comm comm_world ) {

    // Open the NETCDF file
    int FLAG = NC_NETCDF4 | NC_MPIIO;
    int ncid=0, retval;
    char buffer [50];
    snprintf(buffer, 50, "initial_conditions.nc");
    if (( retval = nc_open_par(buffer, FLAG, comm_world, MPI_INFO_NULL, &ncid) )) { NC_ERR(retval); }

    //
    //// Get simulation parameters
    //
    int Nx_varid;
    if ((retval = nc_inq_varid(ncid, "Nx", &Nx_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Nx_varid, Nx);

    int Ny_varid;
    if ((retval = nc_inq_varid(ncid, "Ny", &Ny_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Ny_varid, Ny);

    int Lx_varid;
    if ((retval = nc_inq_varid(ncid, "Lx", &Lx_varid))) { NC_ERR(retval); }
    nc_get_var_double(ncid, Lx_varid, Lx);

    int Ly_varid;
    if ((retval = nc_inq_varid(ncid, "Ly", &Ly_varid))) { NC_ERR(retval); }
    nc_get_var_double(ncid, Ly_varid, Ly);

    int Nproc_x_varid;
    if ((retval = nc_inq_varid(ncid, "Nproc_x", &Nproc_x_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Nproc_x_varid, Nproc_x);

    int Nproc_y_varid;
    if ((retval = nc_inq_varid(ncid, "Nproc_y", &Nproc_y_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Nproc_y_varid, Nproc_y);

    int Nthreads_solver_varid;
    if ((retval = nc_inq_varid(ncid, "Nthreads_solver", &Nthreads_solver_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Nthreads_solver_varid, Nthreads);

    int gravity_varid;
    if ((retval = nc_inq_varid(ncid, "gravity", &gravity_varid))) { NC_ERR(retval); }
    nc_get_var_double(ncid, gravity_varid, g);

    int H0_varid;
    if ((retval = nc_inq_varid(ncid, "H0", &H0_varid))) { NC_ERR(retval); }
    nc_get_var_double(ncid, H0_varid, H0);

    int f0_varid;
    if ((retval = nc_inq_varid(ncid, "f0", &f0_varid))) { NC_ERR(retval); }
    nc_get_var_double(ncid, f0_varid, f0);

    int visco_varid;
    if ((retval = nc_inq_varid(ncid, "visco", &visco_varid))) { NC_ERR(retval); }
    nc_get_var_double(ncid, visco_varid, visco);

    int final_time_varid;
    if ((retval = nc_inq_varid(ncid, "final_time", &final_time_varid))) { NC_ERR(retval); }
    nc_get_var_double(ncid, final_time_varid, final_time);

    int num_outs_varid;
    if ((retval = nc_inq_varid(ncid, "num_outs", &num_outs_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, num_outs_varid, num_outs);

    int Nthreads_tracker_varid;
    if ((retval = nc_inq_varid(ncid, "Nthreads_tracker", &Nthreads_tracker_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Nthreads_tracker_varid, Nthreads_tracker);

    int num_particles_varid;
    if ((retval = nc_inq_varid(ncid, "num_particles", &num_particles_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, num_particles_varid, num_particles);

    // Determine coordinates on the 2D processor grid
    // root (0) is at (0,0)
    // proc 1 is at (1,0)
    // proc number_processors_x is at (0,1)
    my_rank_x = bRank % *Nproc_x;
    my_rank_y = bRank / *Nproc_x;

    //
    //// Allocate memory for the fields
    //
    my_Nx = *Nx / *Nproc_x;
    my_Ny = *Ny / *Nproc_y;

    my_u[0]    = new double[my_Nx * my_Ny];
    my_v[0]    = new double[my_Nx * my_Ny];
    my_h[0]    = new double[my_Nx * my_Ny];
    my_vort[0] = new double[my_Nx * my_Ny];

    my_x[0] = new double[my_Nx];
    my_y[0] = new double[my_Ny];
    
    //
    //// Get fields from IC file
    //
    
    // Define the dimensions
    int xdimid, ydimid, tdimid;
    if ((retval = nc_inq_dimid(ncid, "x", &xdimid))) { NC_ERR(retval); }
    if ((retval = nc_inq_dimid(ncid, "y", &ydimid))) { NC_ERR(retval); }

    // Define coordinate variables
    int xvarid, yvarid, tvarid;
    if ((retval = nc_inq_varid(ncid, "x", &xvarid))) { NC_ERR(retval); }
    if ((retval = nc_inq_varid(ncid, "y", &yvarid))) { NC_ERR(retval); }

    // Declare variables
    int uvarid, vvarid, hvarid;
    if ((retval = nc_inq_varid(ncid, "u", &uvarid))) { NC_ERR(retval); }
    if ((retval = nc_inq_varid(ncid, "v", &vvarid))) { NC_ERR(retval); }
    if ((retval = nc_inq_varid(ncid, "h", &hvarid))) { NC_ERR(retval); }

    // Get the coordinate variables
    size_t start_x[1], count_x[1];
    start_x[0] = my_Nx * my_rank_x;
    count_x[0] = my_Nx;
    if ((retval = nc_get_vara_double(ncid, xvarid, start_x, count_x, my_x[0]))) { NC_ERR(retval); }

    size_t start_y[1], count_y[1];
    start_y[0] = my_Ny * my_rank_y;
    count_y[0] = my_Ny;
    if ((retval = nc_get_vara_double(ncid, yvarid, start_y, count_y, my_y[0]))) { NC_ERR(retval); }

    // Get u, v, and h
    size_t start[2], count[2];
    start[0] = my_Ny * my_rank_y;
    start[1] = my_Nx * my_rank_x;
    count[0] = my_Ny;
    count[1] = my_Nx;

    if ((retval = nc_get_vara_double(ncid, uvarid, start, count, my_u[0]))) { NC_ERR(retval); }
    if ((retval = nc_get_vara_double(ncid, vvarid, start, count, my_v[0]))) { NC_ERR(retval); }
    if ((retval = nc_get_vara_double(ncid, hvarid, start, count, my_h[0]))) { NC_ERR(retval); }

    // Close the file
    if ((retval = nc_close(ncid))) { NC_ERR(retval); }

}
