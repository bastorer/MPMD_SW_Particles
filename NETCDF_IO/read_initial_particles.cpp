#include "../netcdf_io.hpp"

// Write to netcdf file
void read_initial_particles(
        double ** my_x, double ** my_y,
        double ** my_part_x, double ** my_part_y, double ** my_part_inert,
        double ** my_part_u, double ** my_part_v,
        int bRank, int * Nx, int * Ny, double * Lx, double * Ly,
        double * final_time, int * num_outs,
        int * Nproc_x, int * Nproc_y, int * Nproc_part,
        int * Nthreads_tracker, int * num_particles,
        MPI_Comm comm_world ) {

    // Open the NETCDF file
    int FLAG = NC_NETCDF4 | NC_MPIIO;
    int ncid=0, retval;
    char buffer [50];
    snprintf(buffer, 50, "initial_conditions.nc");
    if (( retval = nc_open_par(buffer, FLAG, comm_world, MPI_INFO_NULL, &ncid) )) { NC_ERR(retval); }

    //
    //// Get simulation parameters
    //
    int Nx_varid;
    if ((retval = nc_inq_varid(ncid, "Nx", &Nx_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Nx_varid, Nx);

    int Ny_varid;
    if ((retval = nc_inq_varid(ncid, "Ny", &Ny_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Ny_varid, Ny);

    int Lx_varid;
    if ((retval = nc_inq_varid(ncid, "Lx", &Lx_varid))) { NC_ERR(retval); }
    nc_get_var_double(ncid, Lx_varid, Lx);

    int Ly_varid;
    if ((retval = nc_inq_varid(ncid, "Ly", &Ly_varid))) { NC_ERR(retval); }
    nc_get_var_double(ncid, Ly_varid, Ly);

    int Nproc_x_varid;
    if ((retval = nc_inq_varid(ncid, "Nproc_x", &Nproc_x_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Nproc_x_varid, Nproc_x);

    int Nproc_y_varid;
    if ((retval = nc_inq_varid(ncid, "Nproc_y", &Nproc_y_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Nproc_y_varid, Nproc_y);

    int Nproc_part_varid;
    if ((retval = nc_inq_varid(ncid, "Nproc_tracker", &Nproc_part_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Nproc_part_varid, Nproc_part);

    int final_time_varid;
    if ((retval = nc_inq_varid(ncid, "final_time", &final_time_varid))) { NC_ERR(retval); }
    nc_get_var_double(ncid, final_time_varid, final_time);

    int num_outs_varid;
    if ((retval = nc_inq_varid(ncid, "num_outs", &num_outs_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, num_outs_varid, num_outs);

    int Nthreads_tracker_varid;
    if ((retval = nc_inq_varid(ncid, "Nthreads_tracker", &Nthreads_tracker_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, Nthreads_tracker_varid, Nthreads_tracker);

    int num_particles_varid;
    if ((retval = nc_inq_varid(ncid, "num_particles", &num_particles_varid))) { NC_ERR(retval); }
    nc_get_var_int(ncid, num_particles_varid, num_particles);

    //
    //// Allocate memory for the fields
    //
    int my_num_parts = *num_particles / *Nproc_part;

    my_part_x[0]     = new double[my_num_parts];
    my_part_y[0]     = new double[my_num_parts];
    my_part_inert[0] = new double[my_num_parts];
    my_part_u[0]     = new double[my_num_parts];
    my_part_v[0]     = new double[my_num_parts];

    my_x[0] = new double[*Nx];
    my_y[0] = new double[*Ny];
    
    //
    //// Get fields from IC file
    //
    
    // Define the dimensions
    int xdimid, ydimid, tdimid;
    if ((retval = nc_inq_dimid(ncid, "x", &xdimid))) { NC_ERR(retval); }
    if ((retval = nc_inq_dimid(ncid, "y", &ydimid))) { NC_ERR(retval); }

    // Define coordinate variables
    int xvarid, yvarid, tvarid;
    if ((retval = nc_inq_varid(ncid, "x", &xvarid))) { NC_ERR(retval); }
    if ((retval = nc_inq_varid(ncid, "y", &yvarid))) { NC_ERR(retval); }

    // Declare variables
    int partx_varid, party_varid, parti_varid, partu_varid, partv_varid;
    if ((retval = nc_inq_varid(ncid, "particles_x",     &partx_varid))) { NC_ERR(retval); }
    if ((retval = nc_inq_varid(ncid, "particles_y",     &party_varid))) { NC_ERR(retval); }
    if ((retval = nc_inq_varid(ncid, "particles_inert", &parti_varid))) { NC_ERR(retval); }
    if ((retval = nc_inq_varid(ncid, "particles_u",     &partu_varid))) { NC_ERR(retval); }
    if ((retval = nc_inq_varid(ncid, "particles_v",     &partv_varid))) { NC_ERR(retval); }

    // Get the coordinate variables
    if ((retval = nc_get_var_double(ncid, xvarid, my_x[0]))) { NC_ERR(retval); }
    if ((retval = nc_get_var_double(ncid, yvarid, my_y[0]))) { NC_ERR(retval); }

    // Get u, v, and h
    size_t start[1], count[1];
    start[0] = my_num_parts * bRank;
    count[0] = my_num_parts;

    if ((retval = nc_get_vara_double(ncid, partx_varid, start, count, my_part_x[0])    )) { NC_ERR(retval); }
    if ((retval = nc_get_vara_double(ncid, party_varid, start, count, my_part_y[0])    )) { NC_ERR(retval); }
    if ((retval = nc_get_vara_double(ncid, parti_varid, start, count, my_part_inert[0]))) { NC_ERR(retval); }
    if ((retval = nc_get_vara_double(ncid, partu_varid, start, count, my_part_u[0])    )) { NC_ERR(retval); }
    if ((retval = nc_get_vara_double(ncid, partv_varid, start, count, my_part_v[0])    )) { NC_ERR(retval); }

    // Close the file
    if ((retval = nc_close(ncid))) { NC_ERR(retval); }

}
