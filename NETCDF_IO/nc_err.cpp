#include "../netcdf_io.hpp"

// Short-hand function to handling errors
void NC_ERR(int e) {
    fprintf(stderr, "Error: %s\n", nc_strerror(e));
}

