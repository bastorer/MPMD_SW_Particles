#include "interpolators.hpp"

int get_index(int II, int JJ, int Nx, int Ny, int npx, int npy) {

    int nxpp = Nx / npx;
    int nypp = Ny / npy;

    int gx = II / nxpp;
    int gy = JJ / nypp;

    int new_ind = nxpp * nypp * (gx + npx*gy) + (II - gx * nxpp) + nxpp*(JJ - gy * nypp);

    return new_ind;
}

double cubic_interp_1d(double f_m1, double f_0, double f_1, double f_2, double interp_pt) {
    // This cubic interpolant approximates the funcation and first derivative between [0,1]
    //   given points at [-1, 0, 1, 2].
    double a = -0.5*f_m1 + 1.5*f_0 - 1.5*f_1 + 0.5*f_2;
    double b =      f_m1 - 2.5*f_0 +   2*f_1 - 0.5*f_2;
    double c = -0.5*f_m1           + 0.5*f_1;
    double d =                 f_0;
    return a*pow(interp_pt,3) + b*pow(interp_pt,2) + c*interp_pt + d;
}

double cubic_interp(double * func, double xp, double yp, double dx, double dy, int Nx, int Ny,
        int npx, int npy, int Ppp) {
    
    // Get indices for the box containing the point (xp, yp)
    int II = fmod( fmod( floor( (xp - dx/2.) / dx), Nx) + Nx, Nx ) ;
    int JJ = fmod( fmod( floor( (yp - dy/2.) / dy), Ny) + Ny, Ny ) ;

    if ( (II < 0) or (II >= Nx)) { fprintf(stderr, "tracker: ERROR: II = %d (xp = %g, yp = %g)\n", II, xp, yp); }
    if ( (JJ < 0) or (JJ >= Ny)) { fprintf(stderr, "tracker: ERROR: JJ = %d (xp = %g, yp = %g)\n", JJ, xp, yp); }

    double dx_star = xp - dx * (II + 0.5);
    if (xp < dx/2.) { dx_star = dx/2. + xp; }
    if ( (dx_star < 0) or (dx_star > dx)) { fprintf(stderr, "tracker: ERROR: dx_star/dx = %g, xp = %g, II = %d\n", dx_star/dx, xp, II); }

    double dy_star = yp - dy * (JJ + 0.5);
    if (yp < dy/2.) { dy_star = dy/2. + yp; }
    if ( (dy_star < 0) or (dy_star > dy)) { fprintf(stderr, "tracker: ERROR: dy_star/dy = %g, yp = %g, JJ = %d\n", dy_star/dy, yp, JJ); }

    double f_interp = 
        cubic_interp_1d( 
          cubic_interp_1d( func[ (II-1) % Nx + ((JJ-1) % Ny) * Nx], 
                           func[  II         + ((JJ-1) % Ny) * Nx],
                           func[ (II+1) % Nx + ((JJ-1) % Ny) * Nx],
                           func[ (II+2) % Nx + ((JJ-1) % Ny) * Nx],
                           dx_star / dx),
          cubic_interp_1d( func[ (II-1) % Nx + ( JJ        ) * Nx], 
                           func[  II         + ( JJ        ) * Nx],
                           func[ (II+1) % Nx + ( JJ        ) * Nx],
                           func[ (II+2) % Nx + ( JJ        ) * Nx],
                           dx_star / dx),
          cubic_interp_1d( func[ (II-1) % Nx + ((JJ+1) % Ny) * Nx], 
                           func[  II         + ((JJ+1) % Ny) * Nx],
                           func[ (II+1) % Nx + ((JJ+1) % Ny) * Nx],
                           func[ (II+2) % Nx + ((JJ+1) % Ny) * Nx],
                           dx_star / dx),
          cubic_interp_1d( func[ (II-1) % Nx + ((JJ+2) % Ny) * Nx], 
                           func[  II         + ((JJ+2) % Ny) * Nx],
                           func[ (II+1) % Nx + ((JJ+2) % Ny) * Nx],
                           func[ (II+2) % Nx + ((JJ+2) % Ny) * Nx],
                           dx_star / dx),
          dy_star / dy);

    return f_interp;

}

double linear_interp(double * func, double xp, double yp, double dx, double dy, int Nx, int Ny,
        int npx, int npy, int Ppp) {

    // Get indices for the box containing the point (xp, yp)
    int II = fmod( fmod( floor( (xp - dx/2.) / dx), Nx) + Nx, Nx ) ;
    int JJ = fmod( fmod( floor( (yp - dy/2.) / dy), Ny) + Ny, Ny ) ;

    if ( (II < 0) or (II >= Nx)) { fprintf(stderr, "tracker: ERROR: II = %d\n", II); }
    if ( (JJ < 0) or (JJ >= Ny)) { fprintf(stderr, "tracker: ERROR: JJ = %d\n", JJ); }

    // Set indicies
    // dx_star is the x distance from xL (xLeft)
    double dx_star = xp - dx * (II + 0.5);
    if (xp < dx/2.) { dx_star = dx/2. + xp; }
    if ( (dx_star < 0) or (dx_star > dx)) { fprintf(stderr, "tracker: ERROR: dx_star/dx = %g, xp = %g, II = %d\n", dx_star/dx, xp, II); }

    // dy_star is the y distance from yL (yLower)
    double dy_star = yp - dy * (JJ + 0.5);
    if (yp < dy/2.) { dy_star = dy/2. + yp; }
    if ( (dy_star < 0) or (dy_star > dy)) { fprintf(stderr, "tracker: ERROR: dy_star/dy = %g, yp = %g, JJ = %d\n", dy_star/dy, yp, JJ); }

    double f_interp = 
          ( 1 - dy_star / dy  ) * 
              (   (1 - dx_star/dx ) * func[get_index(  II,          JJ,         Nx, Ny, npx, npy)]
                + (    dx_star/dx ) * func[get_index( (II+1) % Nx,  JJ,         Nx, Ny, npx, npy)] )
        + ( dy_star / dy ) * 
              (   (1 - dx_star/dx ) * func[get_index(  II,         (JJ+1) % Ny, Nx, Ny, npx, npy)]
                + (    dx_star/dx ) * func[get_index( (II+1) % Nx, (JJ+1) % Ny, Nx, Ny, npx, npy)] );

    // Return interpolation
    return f_interp;
}
